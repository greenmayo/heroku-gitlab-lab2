// server/index.js
const users = new Array();
const express = require("express");
const dotenv = require('dotenv');
const PORT = process.env.PORT || 3001;

const app = express();

const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.REACT_APP_GOOGLE_CLIENT_ID)
dotenv.config();
app.use(express.json());

app.post("/api/v1/auth/google", async (req, res) => {
  const user = {"name" : name, "email": email, "picture": picture};
  const existsAlready = users.findIndex(element => element.email === email);
  const { token } = req.body    
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.REACT_APP_GOOGLE_CLIENT_ID
  });
  const { name, email, picture } = ticket.getPayload(); 
  const user = await db.user.upsert({
    where: { email: email },
    update: { name, picture },
    create: { name, email, picture }
  })    
  res.status(201)
  res.json(user)
})

app.get("/api", (req, res) => {
  
  res.json({ message: "Hello from server! "});
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});